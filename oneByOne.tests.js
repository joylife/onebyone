(function () {
    "use strict";

    var stream = require("stream");
    var assert = require("chai").assert;

    var oneByOne = require("./");

    describe("onyByOne", function () {
        describe("fork", function () {
            var fork = oneByOne.fork;

            it("is a simple fork and join mechanism", function (callback) {
                var join = fork(function (callback) {
                    return callback(null, 100);
                });

                join(function (err, val) {
                    assert.ifError(err);
                    assert.strictEqual(val, 100);
                    return setImmediate(callback);
                });
            });

            it("can support multiple joiners", function (callback) {
                var join = fork(function (callback) {
                    return setTimeout(callback.bind(null, null, 100), 500);
                });

                var calls = 1;
                var verifyNextCall = function (err, val) {
                    assert.strictEqual(val, 100);
                    calls++;
                };

                var verifyLastCall = function (err, val) {
                    verifyNextCall(err, val);
                    return setImmediate(callback);
                };

                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyLastCall);

            });

            it("will not fire joiners if the fork call never returns", function (callback) {
                var join = fork(function () {
                });

                var calls = 1;
                var verifyNextCall = function (err, val) {
                    assert.strictEqual(val, 100);
                    calls++;
                };

                var verifyLastCall = function (err, val) {
                    verifyNextCall(err, val);
                    return setImmediate(callback);
                };

                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyNextCall);
                join(verifyLastCall);

                setTimeout(function () {
                    assert.strictEqual(calls, 1);
                    return setImmediate(callback);
                }, 500);

            });
        });
        describe("generator", function () {
            var generateAdd1 = function (initVal) {
                var oldValue = initVal || 0;

                var add1 = function (callback) {
                    oldValue = oldValue + 1;
                    return setTimeout(callback.bind(null, null, oldValue), 50);
                };

                return add1;
            };


            it("acts like a transparent function when there is only one caller", function (callback) {
                var adder = oneByOne.generator(generateAdd1(0));
                adder(function (err, val) {
                    assert.ifError(err);
                    assert.strictEqual(val, 1);

                    adder(function (err, val) {
                        assert.ifError(err);
                        assert.strictEqual(val, 2);
                        return setImmediate(callback);
                    });
                });
            });

            it("serializes calls  made in parellel", function (callback) {
                var adder = oneByOne.generator(generateAdd1(0));

                var calls = 1;
                var verifyNextCall = function (err, val) {
                    assert.strictEqual(val, calls);
                    calls++;
                };

                var verifyLastCall = function (err, val) {
                    verifyNextCall(err, val);
                    return setImmediate(callback);
                };
                adder(verifyNextCall);
                adder(verifyNextCall);
                adder(verifyNextCall);
                adder(verifyNextCall);
                adder(verifyLastCall);
            });

            it("preserves this", function (callback) {
                var testThis = {
                    "someObj": true
                };

                var add1 = generateAdd1(0);
                var boundAdd1 = function (oldValue, callback) {
                    assert.strictEqual(this, testThis);
                    return add1(oldValue, callback);
                }.bind(testThis);

                var adder = oneByOne.generator(boundAdd1, 0);

                var calls = 1;
                var verifyNextCall = function (err, val) {
                    assert.strictEqual(val, calls);
                    calls++;
                };

                var verifyLastCall = function (err, val) {
                    verifyNextCall(err, val);
                    return setImmediate(callback);
                };
                adder(verifyLastCall);
            });
        });
        describe("ObjectStreamIterator", function () {

            it("converts a readable object stream into an iterator", function (callback) {
                var readable = new stream.Readable({highWaterMark: 0, objectMode: true});
                readable._read = function () {
                    readable.push({val: 10});
                    readable.push(null);
                };
                var iter = oneByOne.createObjectStreamIterator(readable);
                iter.onNext(function (err, obj) {
                    assert.ifError(err);
                    assert(obj);
                    assert.strictEqual(obj.val, 10);
                    iter.onNext(function (err, obj) {
                        assert.ifError(err);
                        assert(!obj);
                        return setImmediate(callback);
                    });
                });
            });

            it("doesnot like being pestered with parallel onNext calls", function (callback) {
                var readable = new stream.Readable({highWaterMark: 0, objectMode: true});
                var canPush = false;
                readable._read = function () {
                    canPush = true;
                };
                var iter = oneByOne.createObjectStreamIterator(readable);
                //Register the first handler
                iter.onNext(function (err, obj) {
                    assert.ifError(err);
                    assert(obj);
                    assert.strictEqual(obj.val, 10);
                    iter.onNext(function (err, obj) {
                        assert.ifError(err);
                        assert(!obj);
                        return setImmediate(callback);
                    });
                });

                //Try to register the second handler
                iter.onNext(function (err) {
                    assert(err);
                    return setTimeout(function () {
                        assert.strictEqual(canPush, true);
                        readable.push({val: 10});
                        readable.push(null);
                    }, 100);
                });
            });
        });

        describe("ObjectSourceStream", function () {
            it("can convert empty function to a stream of length 0", function (callback) {
                var objStream = oneByOne.createObjectSourceStream(null, function (callback) {
                    return callback();
                });
                var iter = oneByOne.createObjectStreamIterator(objStream);

                iter.onNext(function (err, data) {
                    assert(!err);
                    assert(!data);
                    return callback();
                });
            });

            it("can convert single object generator to a stream of length 1", function (callback) {
                var itemsToGenerate = 1;
                var objStream = oneByOne.createObjectSourceStream(null, function (callback) {
                    assert(itemsToGenerate);
                    itemsToGenerate--;
                    return callback(null, {}, !itemsToGenerate);
                });
                var iter = oneByOne.createObjectStreamIterator(objStream);

                return iter.onNext(function (err, data) {
                    assert(!err);
                    assert(data);
                    return iter.onNext(function (err, data) {
                        assert(!err);
                        assert(!data);
                        return callback();
                    });
                });
            });

            it("can convert long running object generator to a stream of length ...", function (callback) {
                var itemsToGenerate = 100;
                var itemsToConsume = itemsToGenerate;
                var objStream = oneByOne.createObjectSourceStream(null, function (callback) {
                    assert(itemsToGenerate);
                    itemsToGenerate--;
                    return callback(null, {}, !itemsToGenerate);
                });
                var iter = oneByOne.createObjectStreamIterator(objStream);


                return iter.onNext(function consumeItem(err, data) {
                    assert(!err);
                    if (itemsToConsume === 0) {
                        assert(!data);
                        return callback();
                    }
                    itemsToConsume--;

                    assert(data);
                    return iter.onNext(consumeItem);
                });
            });

        });

        describe("ArraySplitTransform", function () {
            it("can convert a arrays to its individual elements", function (callback) {
                var itemsToGenerate = 100;
                var itemsToTransferPerTurn = 10;
                var itemsToConsume = itemsToGenerate;
                var objStream = oneByOne.createObjectSourceStream(null, function (callback) {
                    assert(itemsToGenerate >= 0);

                    var arrayGenerated;
                    if (itemsToGenerate > itemsToTransferPerTurn) {
                        arrayGenerated = new Array(itemsToTransferPerTurn);
                        itemsToGenerate-=itemsToTransferPerTurn;
                    }
                    else {
                        arrayGenerated = new Array(itemsToGenerate);
                        itemsToGenerate = 0;
                    }

                    for (var i = 0; i < arrayGenerated.length; i++) {
                        arrayGenerated[i] = 0;
                    }

                    return callback(null, arrayGenerated, !itemsToGenerate);
                }).pipe(oneByOne.createArraySplitTransformStream());
                var itr = oneByOne.createObjectStreamIterator(objStream);

                return itr.onNext(function consumeItem(err, data) {
                    if (err) {
                        return callback(err);
                    }
                    if (data === null) {
                        assert.strictEqual(itemsToConsume, 0);
                        return callback();
                    }
                    assert.strictEqual(data, 0);
                    itemsToConsume--;
                    return itr.onNext(consumeItem);

                });
            });

            it("can regulate the pressure on the array source when caching is disabled", function (callback) {
                var itemsToGenerate = 100;
                var itemsToTransferPerTurn = 10;
                var itemsToConsume = itemsToGenerate;
                var objStream = oneByOne.createObjectSourceStream({highWaterMark: 0}, function (callback) {
                    assert(itemsToGenerate >= 0);

                    var arrayGenerated;
                    if (itemsToGenerate > itemsToTransferPerTurn) {
                        arrayGenerated = new Array(itemsToTransferPerTurn);
                        itemsToGenerate-=itemsToTransferPerTurn;
                    }
                    else {
                        arrayGenerated = new Array(itemsToGenerate);
                        itemsToGenerate = 0;
                    }

                    for (var i = 0; i < arrayGenerated.length; i++) {
                        arrayGenerated[i] = 0;
                    }

                    return callback(null, arrayGenerated, !itemsToGenerate);

                }).pipe(oneByOne.createArraySplitTransformStream({highWaterMark: 0}));
                var itr = oneByOne.createObjectStreamIterator(objStream);

                return itr.onNext(function consumeItem(err, data) {
                    if (err) {
                        return callback(err);
                    }
                    if (data === null) {
                        assert.strictEqual(itemsToConsume, 0);
                        return callback();
                    }
                    itemsToConsume--;

                    /**
                     * The pressure dance!!
                     * In this scenario the generator generates an array of 10 objects and then the
                     * array flattener flattens it.
                     *
                     * This check ensures that the array generator is not called if there are already
                     * objects which being processed by the array flattener.
                     *
                     * The range of 20 objects below is because 10 objects are buffered in the source
                     * and the other 10 in the transform, so the source and sink should always be not more
                     * than 20 objects apart
                     */
                    assert(itemsToGenerate <= itemsToConsume);

                    //We are adding 10 more here because node 0.12 is not correctly respecting highWaterMark: 0
                    //It is treating it as highWaterMark: 1
                    assert(itemsToConsume <= itemsToGenerate + 20 + 10);
                    assert.strictEqual(data, 0);

                    return itr.onNext(consumeItem);

                });
            });

            it("can regulate the pressure on the array source when caching is enabled", function (callback) {
                var itemsToGenerate = 200;
                var itemsToTransferPerTurn = 10;
                var itemsToConsume = itemsToGenerate;
                var objStream = oneByOne.createObjectSourceStream({highWaterMark: 3}, function (callback) {
                    assert(itemsToGenerate >= 0);

                    var arrayGenerated;
                    if (itemsToGenerate > itemsToTransferPerTurn) {
                        arrayGenerated = new Array(itemsToTransferPerTurn);
                        itemsToGenerate-=itemsToTransferPerTurn;
                    }
                    else {
                        arrayGenerated = new Array(itemsToGenerate);
                        itemsToGenerate = 0;
                    }

                    for (var i = 0; i < arrayGenerated.length; i++) {
                        arrayGenerated[i] = 0;
                    }

                    return callback(null, arrayGenerated, !itemsToGenerate);
                }).pipe(oneByOne.createArraySplitTransformStream({highWaterMark: 0}));
                var itr = oneByOne.createObjectStreamIterator(objStream);

                return itr.onNext(function consumeItem(err, data) {
                    if (err) {
                        return callback(err);
                    }
                    if (data === null) {
                        assert.strictEqual(itemsToConsume, 0);
                        return callback();
                    }
                    itemsToConsume--;

                    /**
                     * The pressure dance!!
                     * In this scenario the generator generates an array of 100 objects and then the
                     * array flattener flattens it.
                     *
                     * This check ensures that the array generator is not called if there are already
                     * objects which being processed by the array flattener.
                     *
                     * The range of 50 objects below is because 10 + 30 (as {highWaterMark: 3}) objects are buffered
                     * in the source and the other 10 in the transform, so the source and sink should always
                     * be not more than 50 objects apart
                     *
                     */
                    assert(itemsToGenerate <= itemsToConsume);

                    //We are adding 10 more here because node 0.12 is not correctly respecting highWaterMark: 0
                    //It is treating it as highWaterMark: 1
                    assert(itemsToConsume <= itemsToGenerate + 20 + 30 + 10);
                    assert.strictEqual(data, 0);
                    return itr.onNext(consumeItem);

                });
            });
        });
    });
})();
