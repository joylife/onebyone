(function () {
    "use strict";

    exports.createArraySplitTransformStream = require("./_arraySplitTransformStream").createArraySplitTransformStream;
    exports.createObjectMapTransformStream = require("./_objectMapTransformStream").createObjectMapTransformStream;
    exports.createObjectSourceStream = require("./_objectSourceStream").createObjectSourceStream;
    exports.createObjectStreamIterator = require("./_objectStreamIterator").createObjectStreamIterator;

    /**
     *
     * Returns a function which will call fnGenerate serially irrespective of
     * the returned function being called in parallel.
     *
     * Imagine a token machine, queue to get tokens, and the operator Bob.
     * Bob generally sleeps but he can wake up for the following reasons...
     *
     * * When a new person walks into the room
     * * * Bob escorts him to the queue and checks if he is the first person in the queue.
     * * * If he is not the first person Bob goes back to sleep.
     * * * If he is the first person, Bob presses the red button to request the token and then goes to sleep.
     *
     * * When the token machine beeps with a new token
     * * * Bob gives the token to the first person in the queue and sends him away
     * * * If there is no one in the queue Bob goes back to sleep
     * * * Otherwise he presses the redButton again before sleeping!
     *
     * @param fnGenerate - function of the form fnGenerate(err, val)
     * @returns {*}
     */
    var generator = function (fnGenerate) {

        var callbackQ = [];

        var redButton;
        var enterAskingForToken;

        redButton = function () {
            return fnGenerate(function () {
                //When token machine generates the new token

                //Bob gives the token to the first person in the queue and sends him away
                var callback = callbackQ.pop();

                //We want a function which will forward arguments... callback(arg1, arg2, arg3,...);
                //thats callback.bind(null, arg1, arg2, arg3,...);
                //thats callback.bind.call(callback, null, arg1, arg2, arg3, ...);
                //thats callback.bind.apply(callback, [null, args1, args2, args3, ...]);
                //thats args.unshift(null); callback.bind.apply(callback, args);
                var args = Array.prototype.slice.apply(arguments); args.unshift(null);
                setImmediate(callback.bind.apply(callback, args));


                if (callbackQ.length === 0) {
                    return;
                }

                //If he is the first person, Bob presses the red button to request the token and then goes to sleep.
                return setImmediate(redButton);
            });
        };

        //When a new person walks into the room
        enterAskingForToken = function (callback) {
            callbackQ.push(callback);

            //If he is not the first person Bob goes back to sleep.
            if (callbackQ.length > 1) {
                return;
            }

            //If he is the first person, Bob presses the red button to request the token and then goes to sleep.
            return setImmediate(redButton);
        };

        return enterAskingForToken;
    };

    exports.generator = generator;

    /**
     * Starts async execution for fnToFork and return a join function.
     * @param fnToFork
     * @returns {Function}
     */
    var fork = function (fnToFork) {

        var _callbackQ = [];

        var _returned = false;
        var _err = null;
        var _result = null;

        var _fireCallbacksIfReadyAndThereAreAny = function () {
            //Are we ready?
            if (!_returned) {
                return;
            }

            //Are there any?
            if (!_callbackQ.length) {
                return;
            }

            var callbackQ = _callbackQ;
            _callbackQ = [];
            return setImmediate(function () {
                return callbackQ.forEach(function (callback) {
                    return callback(_err, _result);
                });
            });
        };

        var readyToJoin = function (err, result) {
            _returned = true;
            _err = err;
            _result = result;
            return _fireCallbacksIfReadyAndThereAreAny();
        };

        var join = function (callback) {
            _callbackQ.push(callback);
            return _fireCallbacksIfReadyAndThereAreAny();
        };

        setImmediate(fnToFork.bind(null, readyToJoin));
        return join;
    };

    exports.fork = fork;

})();
