(function () {
    "use strict";

    var stream = require("stream");
    var util = require("util");
    var extend = require("extend");

    /**
     * Create a ObjectGeneratorStream stream
     * @param options
     * @param generatorFunction The client can notify us of the generated block, err and end of stream in this callback
     * If err, then we will emit err and assume stram has ended
     * Else we will push the data
     * If data != null && isEnd we will also push an extra null
     * @constructor
     */
    var ObjectGeneratorStream = function (options, generatorFunction) {
        var This = this;
        ObjectGeneratorStream.super_.call(This, extend(true, {}, options, {objectMode: true}));

        This._generatorFunction = generatorFunction;
        This._ended = null;
    };
    util.inherits(ObjectGeneratorStream, stream.Readable);


    ObjectGeneratorStream.prototype._read = function () {
        var This = this;

        /**
         * When we call push node will call read if the buffered data is less than
         * the highWaterMark. We need to ignore these if the client has already
         * notified us that the stream is ending!
         */
        if (This._ended) {
            return;
        }

        var generatorFunc = This._generatorFunction;
        return generatorFunc(function (err, data, isEnd) {
            if (This._ended) {
                throw new Error("Cannot call this callback after the stream has ended!!");
            }
            //This will be the last call if we are in error state or we have no data or we have data but isEnd is set.
            This._ended = !!err || !data || !!isEnd;

            if (err) {
                This.emit("error", err);
                return;
            }

            This.push(data);
            if (!!data && isEnd) {
                This.push();
            }
        });
    };

    exports.createObjectSourceStream = function (options, generatorFunction) {
        return new ObjectGeneratorStream(options, generatorFunction);
    };

})();
