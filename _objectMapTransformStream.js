(function () {
    "use strict";

    var stream = require("stream");
    var util = require("util");
    var extend = require("extend");


    var ObjectMapTransform = function (options, mapFunction) {
        var This = this;
        ObjectMapTransform.super_.call(This, extend(true, {}, options, {objectMode: true}));
        This._mapFunction = mapFunction;
        This._finalizeBlockCount = 0;
        This._finalizeCallback = null;
    };
    util.inherits(ObjectMapTransform, stream.Transform);

    ObjectMapTransform.prototype._blockFinalization = function () {
        var This = this;
        This._finalizeBlockCount++;
    };
    ObjectMapTransform.prototype._unblockFinalization = function () {
        var This = this;
        This._finalizeBlockCount--;
        //console.log("unblock", This._finalizeBlockCount);
        if (This._finalizeBlockCount === 0) {
            if (This._finalizeCallback) {
                //console.log("finalizing");
                return setImmediate(This._finalizeCallback);
            }
        }
    };

    ObjectMapTransform.prototype._transform = function (chunk, encoding, callback) {
        var This = this;
        var mapFunction = This._mapFunction;
        This._blockFinalization();
        mapFunction(chunk, function (err, mapData) {

            if (err) {
                console.log(err);
                This._unblockFinalization();
                return;
            }
            This.push(mapData);
            This._unblockFinalization();
        });
        callback();
    };

    ObjectMapTransform.prototype._flush = function (callback) {
        var This = this;
        This._blockFinalization();
        This._finalizeCallback = callback;
        This._unblockFinalization();
    };


    exports.createObjectMapTransformStream = function (options, mapFunction) {
        return new ObjectMapTransform(options, mapFunction);
    };
})();