(function () {
    "use strict";

    var stream = require("stream");
    var util = require("util");
    var extend = require("extend");

    var ArraySplitTransform = function (options) {
        var This = this;
        ArraySplitTransform.super_.call(This, extend(true, {}, options, {objectMode: true}));
    };
    util.inherits(ArraySplitTransform, stream.Transform);

    ArraySplitTransform.prototype._transform = function (chunk, encoding, callback) {
        var This = this;
        if (typeof chunk === "undefined" || chunk === null) {
            This.push(null);
            return callback();
        }
        chunk.forEach(function (entry) {
            This.push(entry);
        });
        return callback();
    };


    exports.createArraySplitTransformStream = function (options) {
        return new ArraySplitTransform(options);
    };
})();