(function () {
    "use strict";

    var error = require("@withjoy/error");

    /**
     *
     * Exposes a iterator like api to process objects being emitted out of a readable stream
     *
     * @param stream - a readable stream object
     * @constructor
     */
    var ObjectStreamIterator = function (stream) {
        var This = this;
        This._stream = stream;

        This._dataAvailable = false;
        This._nextCallback = null;
        This._ended = false;
        This._endedWithError = null;

        stream.on("readable", This._onReadable.bind(This));
        stream.on("error", This._onError.bind(This));
        stream.on("end", This._onEnd.bind(This));
    };

    ObjectStreamIterator.prototype._fireNext = function (err, data) {
        var This = this;
        var callback = This._nextCallback;
        This._nextCallback = null;
        return setImmediate(callback.bind(null, err, data));
    };

    ObjectStreamIterator.prototype._readDataIfRequired = function () {
        var This = this;
        if (!This._nextCallback) {
            return;
        }

        if (This._ended) {
            return This._fireNext(This._endedWithError, null);
        }

        if (!This._dataAvailable) {
            return;
        }

        var data = This._stream.read();
        if (data === null) {
            This._dataAvailable = false;
            return;
        }

        return This._fireNext(null, data);
    };

    ObjectStreamIterator.prototype._onReadable = function () {
        var This = this;
        This._dataAvailable = true;
        This._readDataIfRequired();
    };

    ObjectStreamIterator.prototype._onError = function (err) {
        var This = this;
        This._ended = true;
        This._endedWithError = err;
        This._readDataIfRequired();

    };

    ObjectStreamIterator.prototype._onEnd = function () {
        var This = this;
        This._ended = true;
        This._endedWithError = null;
        This._readDataIfRequired();
    };

    ObjectStreamIterator.prototype.onNext = function (callback) {
        var This = this;
        if (This._nextCallback) {
            return setImmediate(callback.bind(
                null,
                new error.server.NotImplemented("Next cannot be called while it is already in progress")
            ));
        }
        This._nextCallback = callback;
        This._readDataIfRequired();
    };


    exports.createObjectStreamIterator = function (readableObjectStream) {
        return new ObjectStreamIterator(readableObjectStream);
    };

})();
